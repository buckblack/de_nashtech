﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models
{
    public class MessageModel
    {
        [Key]
        public long Id { get; set; }
        public string contentMessage { get; set; }
        public ICollection<MessageDetailModel> messageDetails { get; set; }
    }
}
