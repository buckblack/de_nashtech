﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models
{
    public class MessageDetailModel
    {
        public long messageId { get; set; }
        public virtual MessageModel message { get; set; }
        public int userId { get; set; }
        public virtual UserModel user { get; set; }
    }
}
