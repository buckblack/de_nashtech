﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models
{
    public class NotifyModel
    {
        [Key]
        public long Id { get; set; }
        public string contentNotify { get; set; }
        public int userId { get; set; }
        public virtual UserModel user { get; set; }
    }
}
