﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models
{
    public class PostModel
    {
        [Key]
        public long Id { get; set; }
        public string postTitle { get; set; }
        public string summaryContent { get; set; }
        public string detailContent { get; set; }
        public DateTime dateCreate { get; set; }
        public string pathThumbnail { get; set; }
        public int status { get; set; }
        public ICollection<CommentModel> comments { get; set; }
        public int subCategoriesId { get; set; }
        public virtual SubCategoryModel subCategories { get; set; }
        public int userId { get; set; }
        public virtual UserModel user { get; set; }
        [NotMapped]
        public string userNameCreate { get; set; }
        [NotMapped]
        public string userPathCreate { get; set; }
        [NotMapped]
        public string userAbout { get; set; }
        [NotMapped]
        public List<PostModel> lstPopular { get; set; }
        [NotMapped]
        public List<PostModel> lstRelated { get; set; }
        [NotMapped]
        public List<PostModel> lstRecent { get; set; }
        [NotMapped]
        public List<PostModel> lstRecentComment { get; set; }
        [NotMapped]
        public List<SubCategoryModel> lstSubCategory { get; set; }
        [NotMapped]
        public List<CategoryModel> lstCategory { get; set; }
        [NotMapped]
        public List<CommentModel> lstComment { get; set; }
    }
}
