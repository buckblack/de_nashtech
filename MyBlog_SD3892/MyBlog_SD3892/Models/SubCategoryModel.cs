﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models
{
    public class SubCategoryModel
    {
        [Key]
        public int Id { get; set; }
        public string subCateName { get; set; }
        public string pathThumbnail { get; set; }
        public int categoryId { get; set; }
        public virtual CategoryModel category { get; set; } // virtual keyword is lazy loading
        public ICollection<PostModel> posts { get; set; }
        [NotMapped]
        public List<PostModel> lstPosts { get; set; }
        [NotMapped]
        public long countPost { get; set; }
    }
}
