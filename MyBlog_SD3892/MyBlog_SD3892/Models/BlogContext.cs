﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models
{
    public class BlogContext: DbContext
    {
        public BlogContext(DbContextOptions<BlogContext> options) : base(options)
        {
        }
        public DbSet<CategoryModel> Categories { get; set; }
        public DbSet<CommentModel> Comments { get; set; }
        public DbSet<GuestModel> Guests { get; set; }
        public DbSet<MessageModel> Messages { get; set; }
        public DbSet<MessageDetailModel> MessageDetails { get; set; }
        public DbSet<NotifyModel> Notifies { get; set; }
        public DbSet<PostModel> Posts { get; set; }
        public DbSet<SubCategoryModel> SubCategories { get; set; }
        public DbSet<UserModel> Users { get; set; }
        public DbSet<RoleModel> Roles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=Blog3892;Trusted_Connection=True;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MessageDetailModel>().HasKey(x => new { x.userId, x.messageId });
            modelBuilder.Entity<MessageDetailModel>()
                .HasOne<MessageModel>(x => x.message)
                .WithMany(s => s.messageDetails)
                .HasForeignKey(x => x.messageId);

            modelBuilder.Entity<MessageDetailModel>()
                .HasOne<UserModel>(x => x.user)
                .WithMany(x => x.messageDetails)
                .HasForeignKey(x => x.userId);
        }

    }
}
