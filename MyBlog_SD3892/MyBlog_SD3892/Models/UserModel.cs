﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models
{
    public class UserModel
    {
        [Key]
        public int Id { get; set; }
        public string fullName { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; }
        public string pathAvatar { get; set; }
        public string description { get; set; }
        public string address { get; set; }
        public string password { get; set; }
        public ICollection<PostModel> posts { get; set; }
        public ICollection<NotifyModel> notifies { get; set; }
        public ICollection<MessageDetailModel> messageDetails { get; set; }
        public int roleId { get; set; }
        public virtual RoleModel role { get; set; }
        [NotMapped]
        public List<RoleModel> lstRole { get; set; }
        [NotMapped]
        public List<UserModel> lstUser { get; set; }
    }
}
