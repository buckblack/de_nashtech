﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models.Components
{
    [ViewComponent(Name = "MenuClient")]
    public class MenuComponent: ViewComponent
    {
        private readonly BlogContext _context;
        public MenuComponent(BlogContext context)
        {
            _context = context;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View("~/Views/Home/_menu.cshtml", await _context.Categories.ToListAsync());
        }
    }
}
