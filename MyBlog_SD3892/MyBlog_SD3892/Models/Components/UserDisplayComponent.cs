﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models.Components
{
    [ViewComponent(Name = "UserDisplay")]
    public class UserDisplayComponent: ViewComponent
    {
        private readonly BlogContext _context;
        public UserDisplayComponent(BlogContext context)
        {
            _context = context;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            
            var user = HttpContext.User.Claims;
            UserModel model = new UserModel();
            model.fullName = user.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault().Value;
            model.pathAvatar = user.Where(x => x.Type == "Avatar").FirstOrDefault().Value;
            model.role = new RoleModel();
            model.role.roleName = user.Where(x => x.Type == ClaimTypes.Role).FirstOrDefault().Value;
            model.Id = int.Parse(user.Where(x => x.Type == "UserId").FirstOrDefault().Value);
            return View("~/Views/AdminAuth/_userDisplay.cshtml", model);
        }
    }
}
