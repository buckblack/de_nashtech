﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models
{
    public class CategoryModel
    {
        [Key]
        public int Id { get; set; }
        public string cateName { get; set; }
        public ICollection<SubCategoryModel> subCategories { get; set; }
        [NotMapped]
        public List<SubCategoryModel> lstSubCategories { get; set; }
        [NotMapped]
        public List<PostModel> lstPost { get; set; }
    }
}
