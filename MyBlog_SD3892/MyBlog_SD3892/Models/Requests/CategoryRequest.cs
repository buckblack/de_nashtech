﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models.Requests
{
    public class CategoryRequest
    {
        public int cateId { get; set; }
        public string cateName { get; set; }
    }
}
