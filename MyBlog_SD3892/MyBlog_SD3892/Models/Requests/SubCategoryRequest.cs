﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models.Requests
{
    public class SubCategoryRequest
    {
        public int subCateId { get; set; }
        public int cateId { get; set; }
        public string subCateName { get; set; }
        public string cateName { get; set; }
        public IFormFile thumbnailSubCate { get; set; }
    }
}
