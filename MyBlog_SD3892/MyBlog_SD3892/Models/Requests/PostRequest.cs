﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models.Requests
{
    public class PostRequest
    {
        public long Id { get; set; }
        public string postTitle { get; set; }
        public string summaryContent { get; set; }
        public string detailContent { get; set; }
        public IFormFile pathThumbnail { get; set; }
        public List<IFormFile> lstImgContent { get; set; }
        public int subCateId { get; set; }
    }
}
