﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models.Requests
{
    public class CommentRequest
    {
        public string guestName { get; set; }
        public string guestEmail { get; set; }
        public string contentComment { get; set; }
        public long postId { get; set; }
    }
}
