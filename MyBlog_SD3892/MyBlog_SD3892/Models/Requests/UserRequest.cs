﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models.Requests
{
    public class UserRequest
    {
        public int userId { get; set; }
        public int userRoleId { get; set; }
        public string userFullName { get; set; }
        public string userPhone { get; set; }
        public string userEmail { get; set; }
        public string userPassword { get; set; }
        public string userAddress { get; set; }
        public string userDescription { get; set; }
        public IFormFile userPathAvatar { get; set; }
    }
}
