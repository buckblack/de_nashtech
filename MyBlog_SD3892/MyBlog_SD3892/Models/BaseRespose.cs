﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog_SD3892.Models
{
    public class BaseRespose
    {
        public int errorCode { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }
}
