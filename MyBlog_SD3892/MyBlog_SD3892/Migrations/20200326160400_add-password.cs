﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyBlog_SD3892.Migrations
{
    public partial class addpassword : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "passWord",
                table: "Users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "passWord",
                table: "Users");
        }
    }
}
