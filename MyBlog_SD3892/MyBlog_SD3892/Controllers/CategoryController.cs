﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyBlog_SD3892.Models;

namespace MyBlog_SD3892.Controllers
{
    public class CategoryController : Controller
    {
        private readonly BlogContext _context;
        public CategoryController(BlogContext context)
        {
            _context = context;
        }
        public IActionResult Index(string cateId)
        {
            if(string.IsNullOrEmpty(cateId))
            {
                return RedirectToAction("Index", "Home");
            }
            CategoryModel model = _context.Categories.Where(x => x.Id == int.Parse(cateId)).Include(x => x.subCategories)
                .ThenInclude((SubCategoryModel o) => o.posts).FirstOrDefault();
            if(model != null)
            {
                List<PostModel> lstPost = new List<PostModel>();
                foreach (SubCategoryModel sub in model.subCategories)
                {
                    lstPost.AddRange(sub.posts);
                }

                model.lstPost = _context.Posts.Where(x => x.subCategories.categoryId == int.Parse(cateId)).ToList();
                model.lstPost.OrderByDescending(x => x.dateCreate);
                return View(model);
            }
            return View();
        }

        public IActionResult SubCategory(string subCateId)
        {
            if (string.IsNullOrEmpty(subCateId))
            {
                return RedirectToAction("Index", "Category");
            }
            SubCategoryModel model = _context.SubCategories.Where(x => x.Id == int.Parse(subCateId)).Include(x=>x.posts).FirstOrDefault();
            return View(model);
        }
    }
}