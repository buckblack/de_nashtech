﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyBlog_SD3892.Models;
using MyBlog_SD3892.Models.Requests;

namespace MyBlog_SD3892.Controllers
{
    public class DetailController : Controller
    {
        private readonly BlogContext _context;
        public DetailController(BlogContext context)
        {
            _context = context;
        }
        public IActionResult Index(string postId)
        {
            try
            {

                if (string.IsNullOrEmpty(postId))
                {
                    return RedirectToAction("Index", "Category");
                }
                PostModel model = _context.Posts.Where(x => x.Id == long.Parse(postId)).Include(x => x.user).Include(x => x.comments).ThenInclude((CommentModel o) => o.guest).FirstOrDefault();
                if (model == null)
                {
                    return View();
                }
                model.lstSubCategory = _context.SubCategories.ToList();
                model.lstRelated = _context.Posts.Where(x => x.subCategoriesId == model.subCategoriesId && x.Id != model.Id).ToList();
                model.lstPopular = _context.Posts.OrderBy(x => x.comments.OrderByDescending(o => o.dateCreate).FirstOrDefault()).Take(5).ToList();
                return View(model);
            }
            catch
            {
                return Redirect("/ErrorPage/PageNotFound");
            }
        }

        public IActionResult Search(string searchKey)
        {
            if (string.IsNullOrEmpty(searchKey))
            {
                return RedirectToAction("Index", "Category");
            }
            List<PostModel> model = _context.Posts.Where(x => x.postTitle.Contains(searchKey) || x.summaryContent.Contains(searchKey) || x.detailContent.Contains(searchKey))
                .Include(x => x.subCategories).Include(x => x.user).ToList();
            if(model==null)
            {
                return View();
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult submitComment(CommentRequest comment)
        {
            GuestModel model = _context.Guests.Where(x => x.email == comment.guestEmail.Trim()).FirstOrDefault();
            if(model == null)
            {
                _context.Guests.Add(new GuestModel() { email = comment.guestEmail.Trim(), fullName = comment.guestName });
                _context.SaveChanges();
            }
            GuestModel guest = _context.Guests.Where(x => x.email == comment.guestEmail.Trim()).FirstOrDefault();
            _context.Comments.Add(new CommentModel() { contentComment = comment.contentComment, dateCreate = DateTime.Now, guestId = guest.Id, postId = comment.postId });
            _context.SaveChanges();
            return RedirectToAction("Index", "Detail", new { postId = comment.postId });
        }
    }
}