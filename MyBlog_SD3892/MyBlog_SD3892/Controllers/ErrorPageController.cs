﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MyBlog_SD3892.Controllers
{
    public class ErrorPageController : Controller
    {
        public IActionResult PageNotFound()
        {
            return View();
        }
    }
}