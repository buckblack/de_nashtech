﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyBlog_SD3892.Models;

namespace MyBlog_SD3892.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly BlogContext _context;

        public HomeController(ILogger<HomeController> logger, BlogContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            try
            {
                PostModel model = new PostModel();
                model.lstPopular = _context.Posts.OrderByDescending(x => x.comments.OrderByDescending(o => o.dateCreate).FirstOrDefault()).Take(9).Include(x => x.subCategories).ToList();
                model.lstRecent = _context.Posts.OrderByDescending(x => x.dateCreate).Take(12).Include(x => x.subCategories).ToList();
                model.lstRecentComment = _context.Posts.OrderByDescending(x => x.comments.OrderByDescending(o => o.dateCreate).FirstOrDefault()).Take(12).Include(x => x.subCategories).ToList();
                return View(model);
            }
            catch(Exception ex)
            {
                return Json("Data not found or some error. Detail:<br/>" + ex);
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
