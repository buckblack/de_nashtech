﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyBlog_SD3892.Models;
using MyBlog_SD3892.Models.Requests;

namespace MyBlog_SD3892.Controllers
{
    [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
    public class AdminContentController : Controller
    {
        private readonly BlogContext _context;
        public AdminContentController(BlogContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Posts()
        {
            List<CategoryModel> model = new List<CategoryModel>();
            model.Add(new CategoryModel() { Id = 0, cateName = "All" });
            model.AddRange(await _context.Categories.AsNoTracking().ToListAsync());
            ViewData["message"] = TempData["message"];
            return View(model);
        }

        public async Task<IActionResult> Category()
        {
            List<CategoryModel> model = new List<CategoryModel>();
            model.Add(new CategoryModel() { Id = 0, cateName = "All" });
            model.AddRange(await _context.Categories.AsNoTracking().ToListAsync());
            ViewData["message"] = TempData["message"];
            return View(model);
        }

        public async Task<IActionResult> getAllSubCategory(string cateId)
        {
            List<SubCategoryModel> model = new List<SubCategoryModel>();
            model.Add(new SubCategoryModel() { Id = 0, subCateName = "All" });
            model.AddRange(await _context.SubCategories.Where(x => x.categoryId == int.Parse(cateId)).ToListAsync());
            return Json(model);
        }
        [HttpPost]
        public BaseRespose getAllSubCategoryForUpdatePost(string cateId)
        {
            List<SubCategoryModel> model = _context.SubCategories.Where(x => x.categoryId == int.Parse(cateId)).ToList();
            return new BaseRespose() { errorCode = 0, data = model };
        }

        [HttpPost]
        public async Task<IActionResult> addNewCategory([FromForm] CategoryRequest cate)
        {
            if (string.IsNullOrEmpty(cate.cateName))
            {
                TempData["message"] = "Create error";
            }
            else
            {
                _context.Add(new CategoryModel() { cateName = cate.cateName });
                await _context.SaveChangesAsync();
                TempData["message"] = "Create successful";
            }
            return RedirectToAction("Category","AdminContent");
        }

        [HttpPost]
        public async Task<IActionResult> deleteCategory([FromForm] CategoryRequest cate)
        {
            if (string.IsNullOrEmpty(cate.cateId.ToString()))
            {
                TempData["message"] = "Category not found";
            }
            else
            {
                CategoryModel model = _context.Categories.Where(x => x.Id == cate.cateId).Include(x => x.subCategories).FirstOrDefault();
                if (model != null && model.subCategories.Count > 0)
                {
                    TempData["message"] = "Cannot delete this category";
                }
                else
                {
                    _context.Categories.Remove(model);
                    await _context.SaveChangesAsync();
                    TempData["message"] = "Delete successful";
                }
            }
            return RedirectToAction("Category", "AdminContent");
        }

        [HttpPost]
        public async Task<IActionResult> deleteSubCategory([FromForm] SubCategoryRequest subCate)
        {
            if (string.IsNullOrEmpty(subCate.subCateId.ToString()))
            {
                TempData["message"] = "Sub-Category not found";
            }
            else
            {
                SubCategoryModel model = _context.SubCategories.Where(x => x.Id == subCate.subCateId).Include(x => x.posts).FirstOrDefault();
                if (model != null && model.posts.Count > 0)
                {
                    TempData["message"] = "Cannot delete SubCategory";
                }
                else
                {
                    string pathImage = "wwwroot\\images";
                    string oldImageName = pathImage + Path.Combine(model.pathThumbnail);
                    //remove old file
                    if (model.pathThumbnail != "")
                    {
                        if (System.IO.File.Exists(oldImageName))
                        {
                            System.IO.File.Delete(oldImageName);
                        }
                    }
                    _context.SubCategories.Remove(model);
                    await _context.SaveChangesAsync();
                    TempData["message"] = "Delete successful";
                }
            }
            return RedirectToAction("Category", "AdminContent");
        }

        [HttpPost]
        public async Task<IActionResult> updateCategory([FromForm] CategoryRequest cate)
        {
            if (string.IsNullOrEmpty(cate.cateId.ToString()))
            {
                TempData["message"] = "Update error";
            }
            else
            {
                CategoryModel model = _context.Categories.Find(cate.cateId);
                if (model != null)
                {
                    model.cateName = cate.cateName;
                    _context.Categories.Update(model);
                    await _context.SaveChangesAsync();
                    TempData["message"] = "Update successful";
                }
                else
                {
                    TempData["message"] = "Category not found";
                }
            }
            return RedirectToAction("Category", "AdminContent");
        }

        [HttpPost]
        public async Task<IActionResult> addNewSubCategory([FromForm] SubCategoryRequest subCate)
        {
            if (string.IsNullOrEmpty(subCate.subCateName))
            {
                TempData["message"] = "Create error";
            }
            else
            {
                if (subCate.thumbnailSubCate != null)
                {
                    SubCategoryModel model = new SubCategoryModel();
                    model.subCateName = subCate.subCateName;
                    model.categoryId = subCate.cateId;
                    string pathImage = "wwwroot\\images";
                    var uploadFilesPath = Path.Combine(pathImage, "subcategory");

                    if (!Directory.Exists(uploadFilesPath))
                    {
                        Directory.CreateDirectory(uploadFilesPath);
                    }

                    string newFileName = DateTime.Now.ToString("ddMMyyyyhhssmm") + "_" + subCate.thumbnailSubCate.FileName;

                    //get new path
                    string path = pathImage + "\\subcategory\\" + newFileName;

                    //save new file
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        subCate.thumbnailSubCate.CopyTo(stream);
                        model.pathThumbnail = "/subcategory/" + newFileName;
                    }
                    _context.Add(model);
                    await _context.SaveChangesAsync();
                }
                TempData["message"] = "Create successful";
            }
            return RedirectToAction("Category", "AdminContent");
        }

        [HttpPost]
        public async Task<IActionResult> updateSubCategory([FromForm] SubCategoryRequest subCate)
        {
            if (string.IsNullOrEmpty(subCate.subCateId.ToString()))
            {
                TempData["message"] = "Update error";
            }
            else
            {
                SubCategoryModel model = _context.SubCategories.Find(subCate.subCateId);
                if (model != null)
                {
                    model.categoryId = subCate.cateId;
                    model.subCateName = subCate.subCateName;
                    if (subCate.thumbnailSubCate != null)
                    {
                        string pathImage = "wwwroot\\images";
                        var uploadFilesPath = Path.Combine(pathImage, "subcategory");

                        if (!Directory.Exists(uploadFilesPath))
                        {
                            Directory.CreateDirectory(uploadFilesPath);
                        }
                        //get old path
                        string oldImageName = pathImage + Path.Combine(model.pathThumbnail);

                        string newFileName = DateTime.Now.ToString("ddMMyyyyhhssmm") + "_" + subCate.thumbnailSubCate.FileName;

                        //lấy đường dẫn file mới
                        string path = pathImage + "\\subcategory\\" + newFileName;

                        //xóa file cũ
                        if (model.pathThumbnail != "")
                        {
                            if (System.IO.File.Exists(oldImageName))
                            {
                                System.IO.File.Delete(oldImageName);
                            }
                        }

                        //lưu file mới
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            subCate.thumbnailSubCate.CopyTo(stream);
                            model.pathThumbnail = "/subcategory/" + newFileName;
                        }
                    }
                    _context.SubCategories.Update(model);
                    await _context.SaveChangesAsync();
                    TempData["message"] = "Create successful";
                }
                else
                {
                    TempData["message"] = "SubCategory not found";
                }
            }
            return RedirectToAction("Category", "AdminContent");
        }

        [HttpPost]
        public BaseRespose getCategoryResult(string cateId)
        {
            if(cateId == "0")
            {
                return new BaseRespose() { errorCode = 0, data = _context.Categories.ToList() };
            }
            BaseRespose result= new BaseRespose() { errorCode = 0, data = _context.SubCategories.Where(x => x.categoryId == int.Parse(cateId)).Include(x => x.category).ToList() };
            return result;
        }
        public IActionResult Comments()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> addNewPost([FromForm] PostRequest post)
        {
            if (string.IsNullOrEmpty(post.subCateId.ToString()))
            {
                TempData["message"] = "Create post error";
            }
            else
            {
                PostModel model = new PostModel();
                model.postTitle = post.postTitle;
                model.summaryContent = post.summaryContent;
                model.detailContent = post.detailContent;
                model.dateCreate = DateTime.Now;
                model.subCategoriesId = post.subCateId;
                model.userId = int.Parse(HttpContext.User.Claims.Where(x => x.Type == "UserId").FirstOrDefault().Value);
                if (post.pathThumbnail != null)
                {
                    string pathImage = "wwwroot\\images";
                    var uploadFilesPath = Path.Combine(pathImage, "posts");

                    if (!Directory.Exists(uploadFilesPath))
                    {
                        Directory.CreateDirectory(uploadFilesPath);
                    }

                    string newFileName = DateTime.Now.ToString("ddMMyyyyhhssmm") + "_" + post.pathThumbnail.FileName;

                    //get new path
                    string path = pathImage + "\\posts\\" + newFileName;

                    //save new file
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        post.pathThumbnail.CopyTo(stream);
                        model.pathThumbnail = "/posts/" + newFileName;
                    }
                }
                _context.Add(model);
                await _context.SaveChangesAsync();
                TempData["message"] = "Create successful";
            }
            return RedirectToAction("Posts", "AdminContent");
        }
        [HttpPost]
        public async Task<IActionResult> updatePost([FromForm] PostRequest post)
        {
            if (string.IsNullOrEmpty(post.subCateId.ToString()))
            {
                TempData["message"] = "Update post error";
            }
            else
            {
                PostModel model = _context.Posts.Find(post.Id);
                if (model != null)
                {
                    model.postTitle = post.postTitle;
                    model.summaryContent = post.summaryContent;
                    model.detailContent = post.detailContent;
                    model.subCategoriesId = post.subCateId;
                    model.userId = int.Parse(HttpContext.User.Claims.Where(x => x.Type == "UserId").FirstOrDefault().Value);
                    if (post.pathThumbnail != null)
                    {
                        string pathImage = "wwwroot\\images";
                        string oldImageName = pathImage + Path.Combine(model.pathThumbnail);
                        var uploadFilesPath = Path.Combine(pathImage, "posts");

                        if (!Directory.Exists(uploadFilesPath))
                        {
                            Directory.CreateDirectory(uploadFilesPath);
                        }

                        string newFileName = DateTime.Now.ToString("ddMMyyyyhhssmm") + "_" + post.pathThumbnail.FileName;

                        //get new path
                        string path = pathImage + "\\posts\\" + newFileName;

                        if (model.pathThumbnail != "")
                        {
                            if (System.IO.File.Exists(oldImageName))
                            {
                                System.IO.File.Delete(oldImageName);
                            }
                        }
                        //save new file
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            post.pathThumbnail.CopyTo(stream);
                            model.pathThumbnail = "/posts/" + newFileName;
                        }
                    }
                    _context.Posts.Update(model);
                    await _context.SaveChangesAsync();
                    TempData["message"] = "Update successful";
                }
                else
                {
                    TempData["message"] = "Post not found";
                }
            }
            return RedirectToAction("Posts", "AdminContent");
        }
        [HttpPost]
        public BaseRespose sortPost()
        {
            return new BaseRespose() { errorCode = 0, data = _context.Posts.Include(x=>x.user).Include(x=>x.subCategories).AsNoTracking().ToList() };
        }
        [HttpPost]
        public BaseRespose sortPostByCategoryId(int cateId)
        {
            if (string.IsNullOrEmpty(cateId.ToString()))
            {
                return new BaseRespose() { errorCode = 0, data = _context.Posts.Include(x => x.user).Include(x => x.subCategories).AsNoTracking().ToList() };
            }
            else
            {
                return new BaseRespose()
                {
                    errorCode = 0,
                    data = _context.Posts.Include(x => x.user).Include(x => x.subCategories).ThenInclude(x => x.category).Where(x => x.subCategories.category.Id == cateId).AsNoTracking().ToList()
                };
            }
        }

        [HttpPost]
        public BaseRespose sortPostBySubCategoryId(int subCateId)
        {
            if (string.IsNullOrEmpty(subCateId.ToString()))
            {
                return new BaseRespose() { errorCode = 0, data = _context.Posts.Include(x => x.user).Include(x => x.subCategories).AsNoTracking().ToList() };
            }
            else
            {
                return new BaseRespose() { errorCode = 0, data = _context.Posts.Include(x => x.user).Include(x => x.subCategories).Where(x => x.subCategories.categoryId == subCateId).AsNoTracking().ToList() };
            }
        }
        [HttpPost]
        public IActionResult GetDetailPostById(long postId)
        {
            PostModel model = _context.Posts.Where(x => x.Id == postId).Include(x => x.subCategories).ThenInclude(x => x.category).FirstOrDefault();
            if(model != null)
            {
                model.lstCategory = _context.Categories.ToList();
                model.lstSubCategory = _context.Categories.Where(x => x.Id == model.subCategories.categoryId)
                    .Include(x => x.subCategories).FirstOrDefault().subCategories.ToList();
            }
            return PartialView("~/Views/Shared/Admin/_dialogPost.cshtml", model);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public BaseRespose addImgContentPost(IFormFile file)
        {
            string result = "";
            if (file == null)
            {
                return new BaseRespose() { errorCode = 1 };
            }
            else
            {
                string pathImage = "wwwroot\\images";
                var uploadFilesPath = Path.Combine(pathImage, "posts\\contents");

                if (!Directory.Exists(uploadFilesPath))
                {
                    Directory.CreateDirectory(uploadFilesPath);
                }

                string newFileName = DateTime.Now.ToString("ddMMyyyyhhssmm") + "_" + file.FileName;

                string path = pathImage + "\\posts\\contents\\" + newFileName;

                //save new file
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
                result = $"<div display='d-block text-center add-new-img-post-content-author'><img style='max-width:100%' class='add-new-img-post-content' src='/images/posts/contents/{newFileName}' /><br/><em>Author</em></div>";
            }

            return new BaseRespose() { errorCode = 0, data = Json(result) };
        }
    }
}