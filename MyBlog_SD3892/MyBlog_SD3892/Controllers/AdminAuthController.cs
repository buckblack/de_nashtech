﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyBlog_SD3892.Models;
using MyBlog_SD3892.Models.Requests;

namespace MyBlog_SD3892.Controllers
{
    [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
    public class AdminAuthController : Controller
    {
        private readonly BlogContext _context;
        public AdminAuthController(BlogContext context)
        {
            _context = context;
        }

        [AllowAnonymous]
        public IActionResult Login()
        {
            ViewData["message"] = TempData["message"];
            return View();
        }

        [AllowAnonymous]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "AdminAuth");
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> LoginUser(UserRequest user)
        {
            UserModel model = new UserModel();
            PasswordHasher<UserModel> hash = new PasswordHasher<UserModel>();
            model = _context.Users.Where(x => x.email == user.userEmail).Include(x => x.role).FirstOrDefault();
            if (model != null)
            {

                if (hash.VerifyHashedPassword(model, model.password, user.userPassword) == PasswordVerificationResult.Success)
                {
                    var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
                    identity.AddClaim(new Claim(ClaimTypes.Email, model.email));
                    identity.AddClaim(new Claim(ClaimTypes.Role, model.role.roleName));
                    identity.AddClaim(new Claim("Avatar", model.pathAvatar));
                    identity.AddClaim(new Claim("UserId", model.Id.ToString()));
                    identity.AddClaim(new Claim(ClaimTypes.Name, model.fullName));
                    var principal = new ClaimsPrincipal(identity);
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                }
                else
                {
                    TempData["message"] = "Email or password incorrect";
                }
            }
            else
            {
                TempData["message"] = "Email or password incorrect";
            }
            return RedirectToAction("Index", "AdminDashboard");
        }

        [AllowAnonymous]
        public IActionResult ForgotPW()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult ChangePW()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Users()
        {
            UserModel models = new UserModel();
            models.lstUser = _context.Users.Include(x => x.role).ToList();
            models.lstRole = _context.Roles.ToList();
            return View(models);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Roles()
        {
            ViewData["message"] = TempData["message"];
            return View(_context.Roles.ToList()); ;
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult addNewRole(string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
            {
                TempData["message"] = "Create error";
                return RedirectToAction("Roles", "AdminAuth");
            }
            _context.Roles.Add(new RoleModel() { roleName = roleName });
            _context.SaveChanges();
            return RedirectToAction("Roles", "AdminAuth");
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult addNewUser([FromForm]UserRequest user)
        {
            if (string.IsNullOrEmpty(user.userEmail))
            {
                TempData["message"] = "Create error";
            }
            else
            {
                if (user.userPathAvatar != null)
                {
                    UserModel model = new UserModel();
                    model.password = new PasswordHasher<UserModel>().HashPassword(model, user.userPassword);
                    model.address = user.userAddress;
                    model.description = user.userDescription;
                    model.email = user.userEmail;
                    model.fullName = user.userFullName;
                    model.phoneNumber = user.userPhone;
                    model.roleId = user.userRoleId;
                    string pathImage = "wwwroot\\images";
                    var uploadFilesPath = Path.Combine(pathImage, "users");

                    if (!Directory.Exists(uploadFilesPath))
                    {
                        Directory.CreateDirectory(uploadFilesPath);
                    }

                    string newFileName = DateTime.Now.ToString("ddMMyyyyhhssmm") + "_" + user.userPathAvatar.FileName;

                    //get new path
                    string path = pathImage + "\\users\\" + newFileName;

                    //save new file
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        user.userPathAvatar.CopyTo(stream);
                        model.pathAvatar = "/users/" + newFileName;
                    }
                    _context.Users.Add(model);
                    _context.SaveChangesAsync();
                }
                TempData["message"] = "Create successful";
            }
            return RedirectToAction("Users", "AdminAuth");
        }

    }
}