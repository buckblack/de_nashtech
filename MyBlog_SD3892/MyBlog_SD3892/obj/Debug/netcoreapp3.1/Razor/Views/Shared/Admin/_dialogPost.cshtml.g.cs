#pragma checksum "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3b57d25f10f869322eb0505c6923c89d6f6c5fcf"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_Admin__dialogPost), @"mvc.1.0.view", @"/Views/Shared/Admin/_dialogPost.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\_ViewImports.cshtml"
using MyBlog_SD3892;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\_ViewImports.cshtml"
using MyBlog_SD3892.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3b57d25f10f869322eb0505c6923c89d6f6c5fcf", @"/Views/Shared/Admin/_dialogPost.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b0321cdfcc6d90226f193c35fd4afe4b559fb530", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_Admin__dialogPost : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<MyBlog_SD3892.Models.PostModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<div class=\"modal-body\" id=\"modalBody\">\r\n    <div class=\"form-group\">\r\n        <label for=\"categoryName\">Category Name</label>\r\n        <select id=\"sl_categoryForUpdatePost\" name=\"cateId\" class=\"form-control\"");
            BeginWriteAttribute("onchange", " onchange=\"", 247, "\"", 304, 3);
            WriteAttributeValue("", 258, "changeCategoryUpdatePost(this.value,", 258, 36, true);
#nullable restore
#line 5 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml"
WriteAttributeValue("", 294, Model.Id, 294, 9, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 303, ")", 303, 1, true);
            EndWriteAttribute();
            WriteLiteral(">\r\n");
#nullable restore
#line 6 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml"
             foreach (CategoryModel category in Model.lstCategory)
            {
                var select = Model.subCategories.categoryId == category.Id ? "selected" : "";

#line default
#line hidden
#nullable disable
            WriteLiteral("                ");
            WriteLiteral("<option value=\"");
#nullable restore
#line 9 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml"
                            Write(category.Id);

#line default
#line hidden
#nullable disable
            WriteLiteral("\" ");
#nullable restore
#line 9 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml"
                                          Write(select);

#line default
#line hidden
#nullable disable
            WriteLiteral(" >");
#nullable restore
#line 9 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml"
                                                   Write(category.cateName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</option>\r\n");
#nullable restore
#line 10 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </select>\r\n        ");
#nullable restore
#line 12 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml"
   Write(Html.HiddenFor(m => m.Id));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label for=\"subCategoryName\">Sub-Category Name</label>\r\n        <select id=\"sl_subCategoryForUpdatePost\" name=\"subCateId\" class=\"form-control\">\r\n");
#nullable restore
#line 17 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml"
             foreach (SubCategoryModel subCategory in Model.lstSubCategory)
            {
                var select = Model.subCategories.Id == subCategory.Id ? "selected" : "";

#line default
#line hidden
#nullable disable
            WriteLiteral("                ");
            WriteLiteral("<option value=\"");
#nullable restore
#line 20 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml"
                            Write(subCategory.Id);

#line default
#line hidden
#nullable disable
            WriteLiteral("\" ");
#nullable restore
#line 20 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml"
                                             Write(select);

#line default
#line hidden
#nullable disable
            WriteLiteral(">");
#nullable restore
#line 20 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml"
                                                     Write(subCategory.subCateName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</option>\r\n");
#nullable restore
#line 21 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </select>\r\n        <small id=\"errorCheckSubCate\" class=\"form-text text-danger\"></small>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label for=\"postTitle\">Title</label>\r\n        <input type=\"text\" class=\"form-control\" name=\"postTitle\"");
            BeginWriteAttribute("value", " value=\"", 1375, "\"", 1399, 1);
#nullable restore
#line 27 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml"
WriteAttributeValue("", 1383, Model.postTitle, 1383, 16, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" placeholder=\"Enter title\">\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label for=\"summaryContent\">Summary content</label>\r\n        <input type=\"text\" class=\"form-control\" name=\"summaryContent\"");
            BeginWriteAttribute("value", " value=\"", 1601, "\"", 1630, 1);
#nullable restore
#line 31 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml"
WriteAttributeValue("", 1609, Model.summaryContent, 1609, 21, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" placeholder=""Enter summary content"">
    </div>
    <div class=""form-group"">
        <label for=""exampleInputFile"">Thumbnail</label>
        <div class=""custom-file"">
            <input type=""file"" onChange=""changeCustomChooseFile(this.value)"" class=""custom-file-input"" id=""imageFile"" name=""pathThumbnail"">
            <label class=""custom-file-label"" for=""customFile"">Choose file</label>
        </div>
    </div>
    <div class=""form-group"">
        <label>Content Detail</label><br/>
        <label>More Images</label>
        <input type=""file"" class""form-control"" name=""fileImg"" id=""fileImg"" onChange=""addImgContentPost(this)"">
        <textarea name=""detailContent"" id=""config_binh_luan"" class=""ckeditor"">");
#nullable restore
#line 44 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\Admin\_dialogPost.cshtml"
                                                                         Write(Model.detailContent);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</textarea>
    </div>
</div>
<div class=""modal-footer"" id=""modalFooter"">
    <button type=""button"" class=""btn btn-secondary"" data-dismiss=""modal"">Close</button>
    <button type=""submit"" class=""btn btn-primary"" onclick=""return chechSubCateIdUpdatePost()"">Save</button>
</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<MyBlog_SD3892.Models.PostModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
