#pragma checksum "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\Shared\_footer.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b57eca075d29b55cfb4f267564412cb9989717cf"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__footer), @"mvc.1.0.view", @"/Views/Shared/_footer.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\_ViewImports.cshtml"
using MyBlog_SD3892;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\de_nashtech\MyBlog_SD3892\MyBlog_SD3892\Views\_ViewImports.cshtml"
using MyBlog_SD3892.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b57eca075d29b55cfb4f267564412cb9989717cf", @"/Views/Shared/_footer.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b0321cdfcc6d90226f193c35fd4afe4b559fb530", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__footer : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<div class=""site-footer"">
    <div class=""container"">
        <div class=""row mb-5"">
            <div class=""col-md-4"">
                <h3 class=""footer-heading mb-4"">About Us</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat reprehenderit magnam deleniti quasi saepe, consequatur atque sequi delectus dolore veritatis obcaecati quae, repellat eveniet omnis, voluptatem in. Soluta, eligendi, architecto.</p>
            </div>
            <div class=""col-md-3 ml-auto"">
                <!-- <h3 class=""footer-heading mb-4"">Navigation</h3> -->
                <ul class=""list-unstyled float-left mr-5"">
                    <li><a href=""#"">About Us</a></li>
                    <li><a href=""#"">Advertise</a></li>
                    <li><a href=""#"">Careers</a></li>
                    <li><a href=""#"">Subscribes</a></li>
                </ul>
                <ul class=""list-unstyled float-left"">
                    <li><a href=""#"">Travel</a></li>
                  ");
            WriteLiteral(@"  <li><a href=""#"">Lifestyle</a></li>
                    <li><a href=""#"">Sports</a></li>
                    <li><a href=""#"">Nature</a></li>
                </ul>
            </div>
            <div class=""col-md-4"">


                <div>
                    <h3 class=""footer-heading mb-4"">Connect With Us</h3>
                    <p>
                        <a href=""#""><span class=""icon-facebook pt-2 pr-2 pb-2 pl-0""></span></a>
                        <a href=""#""><span class=""icon-twitter p-2""></span></a>
                        <a href=""#""><span class=""icon-instagram p-2""></span></a>
                        <a href=""#""><span class=""icon-rss p-2""></span></a>
                        <a href=""#""><span class=""icon-envelope p-2""></span></a>
                    </p>
                </div>
            </div>
        </div>
        <div class=""row"">
            <div class=""col-12 text-center"">
                <p>
                    <!-- Link back to Colorlib can't be removed. Template is li");
            WriteLiteral(@"censed under CC BY 3.0. -->
                    Copyright &copy;
                    <script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class=""icon-heart text-danger"" aria-hidden=""true""></i> by <a href=""https://colorlib.com"" target=""_blank"">Colorlib</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </p>
            </div>
        </div>
    </div>
    <!-- move top -->
    <button onclick=""topFunction()"" id=""movetop"" class=""bg-primary"" title=""Go to top"">
        <span class=""icon-angle-up""></span>
    </button>
</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
